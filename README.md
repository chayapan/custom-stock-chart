# U-MARK

An instance of this application is located at this URL: http://umark.ck4.in/

## Project Layout

Main program for U-Mark chart is

*plot.php*

This file is served from PHP web server as HTML5 web application. The page includes HTML partial templates from _partial/ directory, CSS and Javascript from css/ and js/.

The calculation functions are included from

*js/umark.js*

The data series switching loads data from

*series.php*

Available data are stored in CSV format in the data directory

*data/*


The web folder:

```
public_html
├── OHLC.php
├── _partial
│   ├── _chart.php
│   ├── _controls.php
│   ├── _footer.php
│   ├── _head.php
│   └── _navbar.php
├── blank_editor.php
├── blank_page.php
├── css
│   └── style.css
├── data
│   ├── HSI.csv
│   ├── HSI.xlsx
│   ├── SET-Index.csv
│   ├── SET.csv
│   ├── TEST-1.csv
│   └── finance-charts-apple.csv
├── debug_findSetup.php
├── editor.php
├── example_OHLC_controls.php
├── example_Plotly.php
├── index.php
├── js
│   ├── umark.js
│   └── umark_old.js
├── plot.php
└── series.php
```

## plot.php

This is a HTML5 web app built with Bootstrap, jQuery, Plotly.Js and Vue.Js frameworks.

The PHP web page includes partial template files from _partial/ directory.
_head.php
_chart.php
_navbar.php
_footer.php

The custom Javascript functions are included from js/umark.js file.

The web page structure (layout and grid) and navigation menu rely on Bootstrap framework.

The chart plotting uses Plotly.Js library. The plot area is DIV element “umarkPlot”.

The interactive user controls use Vue.Js framework. The controls are inside DIV element “vue-chart-controls”.


## Javascript Functions and Variables

| Function or Variable                              | Located in File         | Description                                                                                                                                        | Example Calls                                                                                                         |
|---------------------------------------------------|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| Function: `plotSeries(symbol)`                   | public_html/plot.php    | Create Plotly.JS plot in DIV element *umarkPlot* in the web page. Load data series from *series.php* according to symbol.     | `plotSeries(“SET”)` File: plot.php Line: 136                                                                         |
| Constant: `currentSeriesPlot`                    | public_html/plot.php    | Stores data and configuration for Plotly.JS chart.                                                                                                 | `const currentSeriesPlot = {data:[], layout:[]}` File: plot.php                                                      |
| Function: `plotSetupPattern(x=4, y=9)`           | public_html/js/umark.js | Calculates setup patterns and makes plot for Buy/Sell Setup. Make calls to the matching functions.                                                 | `plotSetupPattern(this.X, this.Y)` File: plot.php Line 77 Vue.JS action when press “Calculate”.                  |
| Function: `matchBuySetup(X, Y, rows)`            | public_html/js/umark.js | Finds Buy Setup pattern in data rows per X,Y parameters. Each setup marker found is recorded as a dictionary. Returns array of the markers found.  | `setups = matchBuySetup(x, y, currentSeriesPlot.data[0])` File: umark.js Return Buy Setup markers as array.       |
| Function: `matchSellSetup(X, Y, rows)`           | public_html/js/umark.js | Finds Sell Setup pattern in data rows per X,Y parameters. Each setup marker found is recorded as a dictionary. Returns array of the markers found. | `sellSetups = matchSellSetup(x, y, currentSeriesPlot.data[0])` File: umark.js Return Sell Setup markers as array. |
| Function: `plotSetupTrace(foundSetups, plotDiv)` | public_html/js/umark.js | Make the plot of Buy/Sell setup plot from array of markers found. Style the markers, then add the trace to main plot.                              | `setups = buySetups + sellSetups; plotSetupTrace(setups, plotDiv)`                                                    |



## series.php

| Function or Variable     | Located in File            | Description                                                                                             |
|--------------------------|----------------------------|---------------------------------------------------------------------------------------------------------|
| sendData($symbol, $data) | public_html/series.php     | Send data in CSV format to client-side.                                                                 |
| loadData($symbol)        | public_html/series.php     | Load data stored in data/ and call  sendData().                                                  |
| listData()               | public_html/series.php     | List symbols of data series to publish.                                                                 |
| MAIN_ENTRY_POINT         | public_html/series.php | Respond to GET query and return the data series in CSV format if symbol request is in the publish list. |


## Running The Web Application

The application is a PHP web site.
Put everything under public_html/ inside a web hosting folder, and point the client browser to URL of plot.php to use the application.

The default screen is a plot of “SET” data series. Use dropdown select box to change the data series. Press “Calculate” to calculate and plot the markers. (Currently only implement Buy/Sell Setup.)

Using Docker.
Notice the docker-compose file. To run the application with Docker:

```
docker-compose up
```

The development web server with PHP 7.2 serves the application from public_html folder at port 4026.



## Screenshots

Plot of default data series when program loads.
![Default plot shows SET data series](./doc/screenshots/FirstScreen.png)

Change data series.
![Change data series](./doc/screenshots/ChangeSeries.png)

Make calculation and add trace to current plot.
![Setup calculation](./doc/screenshots/CalculateSetup.png)
