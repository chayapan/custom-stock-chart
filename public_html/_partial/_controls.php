<!-- Vue.JS for chart interactive -->
<div id="vue-chart-controls" style="position: absolute; top: 80px; right: 10px;">
  X: {{ X }}  Y: {{ Y }} //
  X: <input v-model.number="X" type="number" placeholder="X">
  Y: <input v-model.number="Y" type="number" placeholder="Y">
  <button v-on:click="calc">Find Setup</button>
  <pre style="white-space: pre-line;">{{ output }}</pre>
</div>

<script>
function rand( ) {
  return Math.random();
}

var chart1 = new Vue({
  el: '#vue-chart-controls',
  data: {
    X: 4,
    Y: 9,
    output: ''
  },
  methods: {
    calc: function() {
      // Remove traces other than the OHLC price line.
      try {
        Plotly.deleteTraces('myPlotly', [1,2])
      }
        catch(err) {
          console.log(err)
      }

      this.refreshPrice()

      // Line 1
      // Array.from({length: 5}, (v, i) => 4) // Array with value 4
      var c1 = Array.from({length: trace.x.length}, (v, i) => "1200")

      var trace2 = {
        // type: "scatter",
        // mode: "lines",
        type: "candlestick",
        name: 'Trace 3',
        x: trace.x,
        open: c1, close: c1, high: c1, low: c1,
        xaxis: 'x',
        yaxis: 'y',
        line: {color: '#7F7F7F'}
      }
      Plotly.addTraces('myPlotly', trace2)

      // Annotate text
      // https://plotly.com/javascript/text-and-annotations/

      sellSetup = findSetup(last275, this.X, this.Y)
      this.output = JSON.stringify(sellSetup)

      var sellSetupTrace = {
        x: [],
        y: [],
        mode: 'markers+text',
        name: 'Sell Setup',
        text: [],
        marker: {
          size: 12,
          color: 'blue',
          symbol: 'star-triangle-down'
        },
        textposition: 'top',
        type: 'scatter'
      }
      // Create plot for sell setup. Plot 1 for every sell setup
      Object.keys(sellSetup).forEach((k, i) => {
        var thisSetup = sellSetup[k]
        // Key, Index
        if ('complete' in thisSetup) {
          console.log("Checking complete setup " + thisSetup.t)
          sellSetupTrace.x.push(thisSetup.date)
          sellSetupTrace.y.push(thisSetup.close)
          sellSetupTrace.text.push("Complete")
        }
      });
      Plotly.addTraces('myPlotly', sellSetupTrace)

      // Plotly.addTraces('myPlotly', {x: trace.x, open: c1, close: c1})
      console.log("calc()")
    },
    updateXY: function() {
      console.log("updateXY()")

    },
    refreshPrice: function() {
        this.output = "ASdfasdf"
        console.log("refreshPrice()")
    }
  }
})
</script>
