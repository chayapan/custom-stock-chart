<!doctype html>
<html lang="en">
  <head>
    <?php $page_title = "Index"; include '_partial/_head.php'; ?>
    <!-- Chart and JS components -->
    <?php include '_partial/_chart.php';?>
    <style>
    .param { width: 50px; }
    #umarkPlot { left: 0px; }
    </style>
  </head>

  <body>
    <!-- Navigation Bar --><?php include '_partial/_navbar.php';?><!-- End Navigation Bar -->

    <!-- Begin page content -->
    <main role="main" class="container">
      <div class="row">
        <div class="col-md-4">
            <h1 class="mt-5">U-Mark Chart</h1>
        </div>
        <div class="col-md-8">
            <!-- Vue.JS for chart interactive -->
            <div id="vue-chart-controls">
              <hr>
              <span>Chart Type:</span>
              <input type="radio" id="chartOHLC" value="ohlc" v-model="chartType" @change="updateChartType">
              <label for="chartOHLC">OHLC</label>
              <input type="radio" id="chartCandle" value="candlestick" v-model="chartType" @change="updateChartType">
              <label for="chartCandle">Candle</label>
              <span>(current={{ chartType }})</span>
              <br>

              <select v-model="series" @change="changeSeries">
                <option disabled value="">Data Series</option>
                <option>SET</option>
                <option>HSI</option>
                <option>TEST-1</option>
              </select>

              {{ fromDate }} to {{ toDate }}

              <button class="btn btn-sm btn-primary" v-on:click="calc">Calculate</button>
              X: {{ X }}  Y: {{ Y }} //
              X: <input class="param" v-model.number="X" type="number" placeholder="X" @change="updateXY">
              Y: <input class="param" v-model.number="Y" type="number" placeholder="Y" @change="updateXY">

              <!-- Output modal -->
              <button type="button" class="btn btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg">Debug output</button>

              <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    Debug output:  hit Find Setup first!
                    <pre style="white-space: pre-line;">{{ output }}</pre>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>

      <script>
      var chart1 = new Vue({
        el: '#vue-chart-controls',
        data: {
          chartType: "ohlc",
          series: '',
          fromDate: '-',
          toDate: '-',
          X: 4,
          Y: 9,
          output: ''
        },
        methods: {
          plotSetup: function() {
              plotSetupPattern(this.X, this.Y)
              this.output = "Calculated." // Note in "Debug output."
          },
          calc: function() {
            // First remove all existing traces except for the data series
            var traceCount = document.getElementById("umarkPlot").data.length
            Plotly.deleteTraces('umarkPlot', _.range([1], traceCount, [1]))

            // plotSeries(this.series) // Load fresh data using D3.js
            plotSetupPattern(this.X, this.Y)
            console.log("calc()")
          },
          updateXY: function() {
            console.log("updateXY()")
            if (this.Y <= this.X) {
              alert("Y can't be less than X. Y must be greater than X.")
              this.Y = this.X +1;
            }
          },
          updateChartType: function() {
            // console.log(currentSeriesPlot.data[0].type)
            // console.log(String(this.chartType))
            currentSeriesPlot.data[0].type = String(this.chartType)
            Plotly.update('umarkPlot', currentSeriesPlot.data, currentSeriesPlot.layout)

            // didn't use watch but see https://github.com/vuejs/vue/issues/293
            var context = this;
            Vue.nextTick(function () {
                console.log(String(context.chartType))
            });
            console.log("updateChartType()")
          },
          changeSeries: function() {
            plotSeries(this.series) // Load new data using D3.js
            console.log("changeSeries()")
          },
          refreshPrice: function() {
              this.output = "TODO: get price update"
              console.log("refreshPrice()")
          }
        }
      });
      </script>
          <div id="umarkPlot" style="width:100%; height:100%; min-width: 1000px; min-height: 600px;"></div>
      <div class="row">
        <div class="col-md-9">
            <p>Controls</p>
        </div>
        <div class="col-md-3">
            <p>Controls</p>
        </div>
      </div>
    </main>

    <script>

    // Reference to the price series under analysis. This is global access.
    const currentSeriesPlot = {data:[], layout:[]}
    // Plot default series.
    plotSeries("SET");
    </script>

    <!-- FOOTER --><?php include '_partial/_footer.php'; ?><!-- END FOOTER -->
  </body>
</html>
