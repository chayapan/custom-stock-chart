<!doctype html>
<html lang="en">
  <head>
    <?php $page_title = "Index"; include '_partial/_head.php'; ?>
    <!-- Chart and JS components -->
    <?php include '_partial/_chart.php';?>

    <!-- Latest compiled and minified plotly.js JavaScript -->
    <script src="https://cdn.plot.ly/plotly-latest.min.js" charset="utf-8"></script>

    <!-- End Chart and JS components -->
  </head>

  <body>
    <!-- Navigation Bar --><?php include '_partial/_navbar.php';?><!-- End Navigation Bar -->

    <!-- Begin page content -->
    <main role="main" class="container">
      <h1 class="mt-5">Sticky footer</h1>
      <p class="lead">Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS.</p>
      <p>Use <a href="../sticky-footer-navbar/">the sticky footer with a fixed navbar</a> if need be, too.</p>

      <div id="myPlotly"/>
    </main>


    <script>
    Plotly.d3.csv('/data/finance-charts-apple.csv', function(err, rows){

    function unpack(rows, key) {
      return rows.map(function(row) {
        return row[key];
      });
    }

    var trace = {
      x: unpack(rows, 'Date'),
      close: unpack(rows, 'AAPL.Close'),
      high: unpack(rows, 'AAPL.High'),
      low: unpack(rows, 'AAPL.Low'),
      open: unpack(rows, 'AAPL.Open'),

      // cutomise colors
      increasing: {line: {color: 'black'}},
      decreasing: {line: {color: 'red'}},

      type: 'ohlc',
      xaxis: 'x',
      yaxis: 'y'
    };

    var data = [trace];

    var layout = {
      dragmode: 'zoom',
      showlegend: false,
      xaxis: {
        rangeslider: {
    		 visible: false
    	 }
      }
    };

    Plotly.newPlot('myPlotly', data, layout);
    });
    </script>



    <!-- FOOTER --><?php include '_partial/_footer.php'; ?><!-- END FOOTER -->
  </body>
</html>
