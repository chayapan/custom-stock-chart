<!doctype html>
<html lang="en">
  <head>
    <?php $page_title = "UMARK"; include '_partial/_head.php'; ?>
    <!-- Chart and JS components -->
    <?php include '_partial/_chart.php';?>
    <!-- End Chart and JS components -->
  </head>

  <body>
    <!-- Navigation Bar --><?php include '_partial/_navbar.php';?><!-- End Navigation Bar -->

    <!-- Begin page content -->
    <main role="main" class="container-fluid">
      <div class="row">
        <h2>OHLC Plot</h2>
  		</div>
      <div id="myPlotly"></div>
    </main>


    <!-- Plot OHLC chart -->
    <script>
    Plotly.d3.csv('/data/SET-Index.csv', function(err, rows){

    function unpack(rows, key) {
      return rows.map(function(row) {
        return row[key];
      });
    }

    var trace = {
      x: unpack(rows, 'Date (GMT)'),
      close: unpack(rows, 'Last'),
      high: unpack(rows, 'High'),
      low: unpack(rows, 'Low'),
      open: unpack(rows, 'Open'),

      // cutomise colors
      increasing: {line: {color: 'green'}},  // black
      decreasing: {line: {color: 'red'}},

      type: 'ohlc',
      xaxis: 'x',
      yaxis: 'y'
    };

    var data = [trace];

    var layout = {
      dragmode: 'zoom',
      showlegend: false,
      xaxis: {
        rangeslider: {
    		 visible: false
    	 }
      }
    };

    Plotly.newPlot('myPlotly', data, layout);
    });
    </script>

    <!-- FOOTER --><?php include '_partial/_footer.php'; ?><!-- END FOOTER -->
  </body>
</html>
