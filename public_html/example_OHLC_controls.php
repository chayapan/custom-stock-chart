<!doctype html>
<html lang="en">
  <head>
    <?php $page_title = "Index"; include '_partial/_head.php'; ?>
    <!-- Chart and JS components -->
    <?php include '_partial/_chart.php';?>
  </head>

  <body>
    <!-- Navigation Bar --><?php include '_partial/_navbar.php';?><!-- End Navigation Bar -->

    <!-- Begin page content -->
    <main role="main" class="container">
      <h1 class="mt-5">U-Mark Chart</h1>

      <!-- Vue.JS for chart interactive -->
      <div id="vue-chart-controls" style="position: absolute; top: 80px; right: 10px;">
        X: {{ X }}  Y: {{ Y }} //
        X: <input v-model.number="X" type="number" placeholder="X">
        Y: <input v-model.number="Y" type="number" placeholder="Y">
        <button v-on:click="calc">Calculate</button>
        <p style="white-space: pre-line;">{{ output }}</p>
      </div>

      <script>

      function rand( ) {
        return Math.random();
      }

      var chart1 = new Vue({
        el: '#vue-chart-controls',
        data: {
          X: 4,
          Y: 9,
          output: ''
        },
        methods: {
          calc: function() {
            // Remove traces other than the OHLC price line.
            try {
              Plotly.deleteTraces('myPlotly', [1,2])
            }
              catch(err) {
                console.log(err)
            }

            this.refreshPrice()

            // Line 1
            // Array.from({length: 5}, (v, i) => 4) // Array with value 4
            var c1 = Array.from({length: trace.x.length}, (v, i) => "1200")

            var trace2 = {
              // type: "scatter",
              // mode: "lines",
              type: "candlestick",
              name: 'Trace 3',
              x: trace.x,
              open: c1, close: c1, high: c1, low: c1,
              xaxis: 'x',
              yaxis: 'y',
              line: {color: '#7F7F7F'}
            }
            Plotly.addTraces('myPlotly', trace2)

            // Line 2
            // JavaScript > Figure Reference
            var c3 = Array.from({length: trace.x.length}, (v, i) => "1000")
            var trace3 = {
              type: "scatter",
              mode: "lines",
              name: 'Line 3',
              x: trace.x,
              y: c3,
              xaxis: 'x',
              yaxis: 'y',
              line: {color: '#7F7F7F'}
            }
            Plotly.addTraces('myPlotly', trace3)


            // Line 4
            // JavaScript > Figure Reference
            // Draw line segment.
            var c4 = Array.from({length: trace.x.length - 500}, (v, i) => "2000")
            var trace4 = {
              type: "scatter",
              mode: "lines",
              name: 'Line 4',
              x: trace.x.reverse().slice(trace.x.length - 500),
              y: c4,
              xaxis: 'x',
              yaxis: 'y',
              line: {color: '#7F7F7F'}
            }
            Plotly.addTraces('myPlotly', trace4)



            // Line 5 - Marker
            // JavaScript > Figure Reference
            // Draw line segment.
            var c4 = Array.from({length: trace.x.length - 500}, (v, i) => "1350")
            var trace4 = {
              type: "scatter",
              mode: "markers",
              name: 'Line 5',
              x: trace.x.slice(trace.x.length - 500),
              y: c4,
              xaxis: 'x',
              yaxis: 'y',
              line: {color: '#7F7F7F'}
            }
            Plotly.addTraces('myPlotly', trace4)


            // Single Marker
            var x0 = trace.x.slice(trace.x.length - 600)[0]
            var marker1 = {
              type: "scatter",
              mode: "markers",
              name: 'Marker 1',
              x: x0,
              y: "1400",
              size: 20,
              xaxis: 'x',
              yaxis: 'y',
              line: {color: '#555'}
            }
            Plotly.addTraces('myPlotly', marker1)

            // https://plotly.com/javascript/text-and-annotations/
            var x0 = trace.x.reverse().slice(-30)
            var y0 = trace.close.reverse().slice(-30)
            var markers2 = {
              x: x0,
              y: Array.from({length: 30}, (v, i) => "2000"),
              mode: 'lines+markers+text',
              name: 'Lines and Text',
              text: y0,
              textposition: 'top left',
              textfont: {
                family: 'sans serif',
                size: 12,
                color: '#ff7f0e'
              },
              type: 'scatter'
            };
            Plotly.addTraces('myPlotly', markers2)

            // Plotly.addTraces('myPlotly', {x: trace.x, open: c1, close: c1})
            console.log("calc()")
          },
          updateXY: function() {
            console.log("updateXY()")
          },
          refreshPrice: function() {

              // Plotly.addTraces('myPlotly', {y: [2,1,2]});

              // Plotly.extendTraces('myPlotly', {close: [[rand()]], open: [[rand()]], high: [[rand()]]}, [0])
              // Plotly.extendTraces('myPlotly', {close: [[0, 3, 5]]}, [0])

              this.output = "ASdfasdf"
              console.log("refreshPrice()")
          }
        }
      })
      </script>


      <div id="myPlotly" style="width:1024px; height:800px;"></div>
    </main>

    <script>
    var trace;
    var data;
    Plotly.d3.csv('/data/SET-Index.csv', function(err, rows){

        function unpack(rows, key) {
          return rows.map(function(row) {
            return row[key];
          });
        }

        trace = {
          name: "Daily Price",
          x: unpack(rows, 'Date (GMT)'),
          close: unpack(rows, 'Last'),
          high: unpack(rows, 'High'),
          low: unpack(rows, 'Low'),
          open: unpack(rows, 'Open'),

          // cutomise colors
          increasing: {line: {color: 'red'}},  // black
          decreasing: {line: {color: 'red'}},

          type: 'ohlc',
          xaxis: 'x',
          yaxis: 'y'
        };

        data = [trace];

        var layout = {
          dragmode: 'zoom',
          showlegend: false,
          xaxis: {
            rangeslider: {
        		 visible: false
        	 }
          }
        };

        Plotly.newPlot('myPlotly', data, layout);
    });
    </script>

    <!-- FOOTER --><?php include '_partial/_footer.php'; ?><!-- END FOOTER -->
  </body>
</html>
