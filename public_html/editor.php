<!doctype html>
<html lang="en">
  <head>
    <?php $page_title = "Index"; include '_partial/_head.php'; ?>
    <!-- Chart and JS components -->
    <?php include '_partial/_chart.php';?>
    <!-- End Chart and JS components -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.1/highlight.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.1/styles/a11y-light.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            min-height: 100vh;
            background: #F6F8F8;
        }

        main {
            max-width: 800px;
            margin: 40px auto;
            padding: 20px;
        }

        .editor {
            background: #fff;
            border-radius: 6px;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
            font-family: "Source Code Pro", monospace;
            font-size: 14px;
            font-weight: 400;
            min-height: 240px;
            letter-spacing: normal;
            line-height: 20px;
            padding: 10px;
            tab-size: 4;
        }
    </style>
  </head>

  <body>
    <!-- Navigation Bar --><?php include '_partial/_navbar.php';?><!-- End Navigation Bar -->

    <!-- Begin page content -->
    <main role="main" class="container">
      <div class="editor language-js"></div>
      <span class="btn-group">
        <button class="btn-sm btn-primary" onclick="run()">Run</button>
        <span id="run-stat">Last executed: </span>
      </span>
    </main>

    <script>
      /*
        Enter something like:
// console.log()
detectSetupPattern(currentSeriesPlot.data[0], 4, 9);
      */


      // Code stored in local storage 'code'
      // Callback for "Run" button
      function run() {
        var code = localStorage.getItem("code");
        eval(code);
        document.getElementById("run-stat").innerHTML = "Last executed: " + (new Date().toLocaleTimeString()).toString()
      }
    </script>

    <script type="module">
  import {CodeJar} from 'https://medv.io/codejar/codejar.js'

  const editor = document.querySelector('.editor')

  const highlight = editor => {
    // highlight.js does not trim old tags,
    // let's do it by this hack.
    editor.textContent = editor.textContent
    hljs.highlightBlock(editor)
  }
  let options = {
    tab: ' '.repeat(2), // default is '\t'
    indentOn: /[(\[]$/, // default is /{$/
  }

  const jar = CodeJar(editor, highlight, options)

  jar.updateCode(localStorage.getItem('code'))
  jar.onUpdate(code => {
    localStorage.setItem('code', code)
  })
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/highlight.min.js"></script>

    <!-- FOOTER --><?php include '_partial/_footer.php'; ?><!-- END FOOTER -->
  </body>
</html>
