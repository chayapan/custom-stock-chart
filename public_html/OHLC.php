<!doctype html>
<html lang="en">
  <head>
    <?php $page_title = "Index"; include '_partial/_head.php'; ?>
    <!-- Chart and JS components -->
    <?php include '_partial/_chart.php';?>
  </head>

  <body>
    <!-- Navigation Bar --><?php include '_partial/_navbar.php';?><!-- End Navigation Bar -->

    <!-- Begin page content -->
    <main role="main" class="container">
      <h1 class="mt-5">U-Mark Chart</h1>
      <!-- CONTROLS --><?php include '_partial/_controls.php'; ?><!-- END CONTROLS -->

      <div id="myPlotly" style="width:1024px; height:800px;"></div>
    </main>

    <script>
    /*
     *  findSetup(last275)
     *
     */

    function findSetup(rows, x, y) {
      console.log("findSetup(" + parseInt(x) + "," +  parseInt(y)+ ")")
      function unpack(rows, key) {
        return rows.map(function(row) {
          return row[key];
        });
      }
      var t = unpack(rows, 'Date (GMT)')
      var close = unpack(rows, 'Last')
      var high = unpack(rows, 'High')
      var low = unpack(rows, 'Low')
      var open = unpack(rows, 'Open')

      var sellSetupCount = 1
      var sellSetup = {}

      // S.1 Find possible sell setup: Close higher than Close X  day ago.
      // Complete sell setup occur when close price remains higher than close of last X day ago for Y.
      //  trace.x[trace.x.length-1] // Today = last day of time series
      rows.forEach((item, i) => {
        // i is iterator that can access close, high, low, open
        // console.log(t[i])
        // console.log(high[i])
        // X days ago

        // Find sell setup
        if (close[i] > close[i-x]) {
          // Find Sell Setup. Close price today higer than close price X day ago.
          console.log("Found Sell Setup")
          // Add to found list
          sellSetup["S" + sellSetupCount.toString()] = {t: i, close: close[i], date: t[i]}

          // Check if sell setup is complete. If X Y
          sellSetupCount += 1 // Increment name
        }

      });

      // S.1 Find complete sell setup. Check if continue to be higher for Y days
      Object.keys(sellSetup).forEach((k, j) => {
        // Key, Index
        console.log("Checking complete " + k)
        var thisSetup = sellSetup[k]
        // last Y close prices
        //  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        // https://stackoverflow.com/questions/8069315/create-array-of-all-integers-between-two-numbers-inclusive-in-javascript-jquer
        var tochk = _.range([x], y+x, [1]) // use lodash
        var iscomplete = true;
        for (c in tochk) {
          if (thisSetup.close < close[thisSetup.t - c]) {
            // console.log("H low. Setup not complete")
            iscomplete = false
          }
        }
        // If still complete after X+Y trace, the setup is complete.
        if (iscomplete) {
          thisSetup["complete"] = true
        }
      });

      var qualifySellSetupCount = 1
      var qualifySellSetup = {}

      // S.2 Find qualified sell setup.
      rows.forEach((item, i) => {
        // Key, Index
        console.log("Checking qualified " + i)
        //Object.keys(sellSetup).forEach((k, j) => {
        //  var thisSetup = sellSetup[k]
        //  high[thisSetup.t]
        //});
      });

      // Returns sell setup with complete flag
      return sellSetup
    }

    var all_price;
    var last275;
    var trace;
    var data;
    var sellSetup;
    Plotly.d3.csv('/data/SET-Index.csv', function(err, rows){

        function unpack(rows, key) {
          all_price = rows
          last275 = _.takeRight(rows,275)
          rows = _.takeRight(rows,275) // Only last 275 days
          return rows.map(function(row) {
            return row[key];
          });
        }

        trace = {
          name: "Daily Price",
          x: unpack(rows, 'Date (GMT)'),
          close: unpack(rows, 'Last'),
          high: unpack(rows, 'High'),
          low: unpack(rows, 'Low'),
          open: unpack(rows, 'Open'),

          // cutomise colors
          increasing: {line: {color: 'red'}},  // black
          decreasing: {line: {color: 'red'}},

          type: 'ohlc',
          xaxis: 'x',
          yaxis: 'y'
        };

        data = [trace];

        var layout = {
          dragmode: 'zoom',
          showlegend: false,
          xaxis: {
            rangeslider: {
        		 visible: false
        	 }
          }
        };

        Plotly.newPlot('myPlotly', data, layout);
    });
    </script>

    <!-- FOOTER --><?php include '_partial/_footer.php'; ?><!-- END FOOTER -->
  </body>
</html>
