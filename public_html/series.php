<?php

$series = array("HSI"=>"HSI", "SET"=>"SET", "SET-Index"=>"SET-Index", "TEST-1"=>"TEST-1");

function listData() {
  global $series;
  echo json_encode($series);
}

function loadData($symbol) {
    // $file = fopen("data/$symbol.csv","r");
    // $data = fgetcsv($file);
    // fclose($file);
    $data = file_get_contents("data/$symbol.csv");
    // echo "here";
    // echo $data; exit();
    sendData($symbol, $data);
}

function sendData($symbol, $data) {
    header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false);
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $symbol . '.csv";');
    header('Content-Transfer-Encoding: binary');
    exit($data);
}


if (isset($_GET["symbol"])) {
    global $series;
    $symbol = $_GET["symbol"];
    if (isset($series[$symbol])) {
      // echo "Loading";
      loadData($symbol); // Return data as CSV
    } else {
      echo json_encode("No Data");
    }
} elseif (isset($_GET["listSeries"])) {
    listData(); // Returns list of data series in JSON format.
}
