
function example_plotSetupPattern() {
  /*  THIS IS EXAMPLE
      [master a154737] Run plotSetupPattern() will plot stub sell and buy
   */

  var sellSetup = { "S1": {"date":"04/20/2015", "close": 1600},
                    "S2": {"date":"04/21/2015", "close": 1600},
                    "S3": {"date":"04/22/2015", "close": 1600},
                  }

  var sellSetupTrace = {
    mode: 'markers+text',
    name: 'Sell Setup',
    x: [],
    y: [],
    text: [],
    textposition: 'top',
    marker: {
      ypad: 20,
      size: 0,     // make invisible
      opacity: 0,  // no marker. Only show text.
      symbol: '0',
      text: {
        size: 14,
        color: "red"
      }
    },
    hoverinfo: "all",
    type: 'scatter'
  }

  var sellSetupLayout = {
    annotations: [

    ]
  }

  // Create plot for sell setup. Plot 1 for every sell setup
  Object.keys(sellSetup).forEach((k, i) => {
    var thisSetup = sellSetup[k]
    // Key, Index
    if ('complete' in thisSetup) {
      console.log("Checking complete setup " + thisSetup.t)
      sellSetupTrace.x.push(thisSetup.date)
      sellSetupTrace.y.push(thisSetup.close)
      sellSetupTrace.text.push("Complete")
    } else { // Those setup that are not complete
      sellSetupTrace.x.push(thisSetup.date)
      sellSetupTrace.y.push(thisSetup.close)
      sellSetupTrace.text.push(k.toString()) // label in key
      console.log(k)
      s = {
        text: thisSetup.k,
        x: 2,
        y: 5,
        xref: 'x', yref: 'y',
      }
      // sellSetupLayout.annotations.push(s)
    }
  });

  Plotly.addTraces(plotDiv, sellSetupTrace)
}



/*
 *  findSetup(last275)  **** OLD VERSION
 *
 */
function findSetupOld(rows, x, y) {
  console.log("findSetup(" + parseInt(x) + "," +  parseInt(y)+ ")")
  function unpack(rows, key) {
    return rows.map(function(row) {
      return row[key];
    });
  }
  var t = unpack(rows, 'Date (GMT)')
  var close = unpack(rows, 'Last')
  var high = unpack(rows, 'High')
  var low = unpack(rows, 'Low')
  var open = unpack(rows, 'Open')

  var sellSetupCount = 1
  var sellSetup = {}

  // S.1 Find possible sell setup: Close higher than Close X  day ago.
  // Complete sell setup occur when close price remains higher than close of last X day ago for Y.
  //  trace.x[trace.x.length-1] // Today = last day of time series
  rows.forEach((item, i) => {
    // i is iterator that can access close, high, low, open
    // console.log(t[i])
    // console.log(high[i])
    // X days ago

    // Find sell setup
    if (close[i] > close[i-x]) {
      // Find Sell Setup. Close price today higer than close price X day ago.
      console.log("Found Sell Setup")
      // Add to found list
      sellSetup["S" + sellSetupCount.toString()] = {t: i, close: close[i], date: t[i]}

      // Check if sell setup is complete. If X Y
      sellSetupCount += 1 // Increment name
    }

  });

  // S.1 Find complete sell setup. Check if continue to be higher for Y days
  Object.keys(sellSetup).forEach((k, j) => {
    // Key, Index
    console.log("Checking complete " + k)
    var thisSetup = sellSetup[k]
    // last Y close prices
    //  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    // https://stackoverflow.com/questions/8069315/create-array-of-all-integers-between-two-numbers-inclusive-in-javascript-jquer
    var tochk = _.range([x], y+x, [1]) // use lodash
    var iscomplete = true;
    for (c in tochk) {
      if (thisSetup.close < close[thisSetup.t - c]) {
        // console.log("H low. Setup not complete")
        iscomplete = false
      }
    }
    // If still complete after X+Y trace, the setup is complete.
    if (iscomplete) {
      thisSetup["complete"] = true
    }
  });

  var qualifySellSetupCount = 1
  var qualifySellSetup = {}

  // S.2 Find qualified sell setup.
  rows.forEach((item, i) => {
    // Key, Index
    console.log("Checking qualified " + i)
    //Object.keys(sellSetup).forEach((k, j) => {
    //  var thisSetup = sellSetup[k]
    //  high[thisSetup.t]
    //});
  });

  // Returns sell setup with complete flag
  return sellSetup
}
