
/*

  umarkPlot                  is DIV element for Plotly chart
  currentSeriesPlot.data[0]  will be the data series trace
*/

console.log("U-Mark Libary loaded")

function rand( ) { return Math.random(); }

/* Get value at lag point */
function lagValue(timeseries, idx, lag) {
  return timeseries.close[idx-lag]
}

function lowerThanLag(timeseries, idx,  lag) {
  v = lagValue(timeseries, idx,  lag)
  if (v < timeseries[idx]) {
    return true
  }
  return false
}

// lastPriceHighest([1,2,3,4,5],5) => true
// lastPriceHighest([1,2,3,4,2],5) => false
// lastPriceHighest([1,2,3,4,5],3) => error
function lastPriceHighest(prices, X) {
    if (prices.length == X) { // OK
      last = prices[X-1]
      before = prices.slice(0, X-1)
      if (last > Math.max(...before)) {
        return true
      }
      return false
    } else { // Should not happen
      console.log("X mismatch: " + String(prices) + " " + String(X))
    }
}

function lastPriceLowest(prices, X) {
    if (prices.length == X) { // OK
      last = prices[X-1]
      before = prices.slice(0, X-1)
      if (last < Math.min(...before)) {
        return true
      }
      return false
    } else { // Should not happen
      console.log("X mismatch: " + String(prices) + " " + String(X))
    }
}


function detectSetupPattern(rows, x, y) {
  console.log("detectSetupPattern(" + parseInt(x) + "," +  parseInt(y)+ ")")
  var setupFound = {"buy":[], "sell":[]}
  var t = rows.x            // Date (GMT)
  var open = rows.open      //Open
  var high = rows.high      // High
  var low = rows.log        // Low
  var close = rows.close    // Last

  //Buy Setup
  blockSize = y

  // Scan left-to-right
  for (i = x; i < t.length ; i++) { // From 0 to Length - X. Exclude first X
    onDate = t[i]
    scanBlock = close.slice(i - x, i - x + y)

    if (scanBlock.length >= y) { // Look at window of size Y
      scanDates = t.slice(i - x, i - x + y)
      var c0 = scanBlock.length
      console.log("Checking: " + onDate + " Block: " + String(scanBlock) + " Dates: " + String(c0) + "::"  + String(scanDates))

      buySetup = []
      j = 1 // counter for within scanBlock
    }
    if (buySetup.length == y) {
      setupFound["buy"].push(buySetup)
    }
    buySetup = [] // Clear count longest sequence
      /*
      // lower than previous X day
      buySetup.push({label: "B" + String(j), day: close[i + x+1], last: close[i + x+1]})
      // shift forward to check the possibility
      for (j; j< blockSize; j++) {
        if (lastPriceLowest(scanBlock.slice(j, j + (x+1)), x + 1)) {
          j += 1
          buySetup.push({label: "B" + String(j), day: close[i + x+1], last: close[i + x+1]})
        } else { // reset pattern
          buySetup = []
        }
      }
      setupFound["buy"].push(buySetup)
      */
  }

  //Sell Setup

  // Return
  return setupFound
}


/*
 *  detectSetupPattern **** OLD VERSION
 */
function scanLeftToRight(rows, x, y) {
  var setupFound = {"buy":[], "sell":[]}
  var t = rows.x            // Date (GMT)
  var open = rows.open      //Open
  var high = rows.high      // High
  var low = rows.log        // Low
  var close = rows.close    // Last

  //Buy Setup
  blockSize = y

  // Scan left-to-right
  for (i = x; i < t.length ; i++) { // From 0 to Length - X. Exclude first X
    onDate = t[i]
    scanBlock = close.slice(i - x, i - x + y)

    if (scanBlock.length >= y) { // Look at window of size Y
      scanDates = t.slice(i - x, i - x + y)
      var c0 = scanBlock.length
      console.log("Checking: " + onDate + " Block: " + String(scanBlock) + " Dates: " + String(c0) + "::"  + String(scanDates))

      buySetup = []
      j = 1 // counter for within scanBlock
    }
    if (buySetup.length == y) {
      setupFound["buy"].push(buySetup)
    }
    buySetup = [] // Clear count longest sequence
  }
}

priceLowest = (prices, last) => {
  if (last < Math.min(...prices)) {
    return true
  }
  return false
}

function setupDetect(x, y) {
    x = 4
    y = 9

    var sellSetupCount = 1
    var SellSetup = {} // new setup

    for (i=0; i<15; i++)
    // For a block of five days. If fifth day is lowest, the setup is found.
    currentSeriesPlot.data[0].x
    days = currentSeriesPlot.data[0].x.slice(0,5)
    price = currentSeriesPlot.data[0].close[5]
    prices = currentSeriesPlot.data[0].close.slice(0,4)
    if (price > Math.max(...prices)) {
      // S1
    }

}

function plotSeries(symbol) {
  var all_price;
  var last275;
  var trace;
  var data;
  var sellSetup;
  Plotly.d3.csv('series.php?symbol='+symbol, function(err, rows){

      function unpack(rows, key) {
        all_price = rows
        // last275 = _.takeRight(rows,275)
        // rows = _.takeRight(rows,275) // Only last 275 days
        return rows.map(function(row) {
          return row[key];
        });
      }

      trace = {
        name: "Daily Price",
        x: unpack(rows, 'Date (GMT)'),
        close: unpack(rows, 'Last'),
        high: unpack(rows, 'High'),
        low: unpack(rows, 'Low'),
        open: unpack(rows, 'Open'),

        // cutomise colors
        increasing: {line: {color: 'red'}},  // black
        decreasing: {line: {color: 'red'}},

        type: 'ohlc',
        xaxis: 'x',
        yaxis: 'y'
      };

      data = [trace];

      var layout = {
        dragmode: 'zoom',      // zoom or pan
        showlegend: false,
        hovermode: "x unified",  // show spikeline and box
        hoverlabel: { align: "right" },
        xaxis: {
          autorange: true,
          title: 'Date',
          showspikes: true,
          spikecolor: "grey",
          spikedash: "dot",
          spikemode: "toaxis+across",
          rangeslider: {
           visible: true
          },
          rangeslider: {
            visible: true,
          },
          rangeselector: {
            x: 0,
            y: 1.2,
            xanchor: 'left',
            font: {size:8},
            buttons: [{
                step: 'month',
                stepmode: 'backward',
                count: 1,
                label: '1 month'
            }, {
                step: 'month',
                stepmode: 'backward',
                count: 6,
                label: '6 months'
            }, {
                step: 'all',
                label: 'All dates'
            }]
          }
        },
        yaxis: {
          showspikes: true,
          spikedash: "dot",
          spikecolor: "grey",
          spikemode: "toaxis+across",
          spikesnap: "hovered data"
        }
      };

      currentSeriesPlot.data = data
      currentSeriesPlot.layout = layout
      Plotly.newPlot('umarkPlot', data, layout);
  });
}

function plotSetupPattern(x=4, y=9) {
    // Annotate text
    // https://plotly.com/javascript/text-and-annotations/

    var plotDiv = 'umarkPlot';
    console.log("plotSetupPattern(" + parseInt(x) + "," +  parseInt(y)+ ")")
    // lookup symbol here: https://plotly.com/javascript/reference/#scatter-marker-symbol
    // hoverinfo and hovertemplate
    // https://plotly.com/javascript/reference/#scatter-hoverinfo
    /* One trace for Sell Setup, one trace for Qualified sell setup */

    /* Access to data at global
          currentSeriesPlot.data[0]
    */

    // Plot 1.  Sell Setup & Buy Setup
    setups = matchSellSetup(x, y, currentSeriesPlot.data[0])
    buySetups = matchBuySetup(x, y, currentSeriesPlot.data[0])

    setups = setups.concat(buySetups)
    plotSetupTrace(setups, plotDiv)
    console.log("Plot sell setup done.")


}

function matchSellSetup(X, Y, rows) {
  var setups = []
  /* Example for debug:
  var sellSetup = { "S1": {"date":"04/20/2016", "close": 1600},
                    "S2": {"date":"04/21/2016", "close": 1600},
                    "S3": {"date":"04/22/2016", "close": 1600},
                  }
  setups.push(sellSetup)
  */
  var t = rows.x            // Date (GMT)
  var open = rows.open      //Open
  var high = rows.high      // High
  var low = rows.log        // Low
  var close = rows.close    // Last

  var c = 0   // Counter for indexing in to the time series
  var ss = 0  // Counter for each setup interval to determine the size/length
  t.forEach((day)=> {
    // console.log(day + " " + c.toString() + " Close:" + close[c].toString()) // Runing from left to right

    // Check Sell Setup
    // Count possible after X days
    if (((c - X) >= 0) && (close[c] > close[c - X])) {
      // if C_t > C_t-x then sell setup (SS) = 1
      ss = 1
      sellSetup = {} // Container for sell setup

      // For i = 1:Y
      // Array of inteters between two values
      var interval = _.range([1], Y + 1, [1]) // use lodash => [1, 2, 3, 4, 5, 6, 7, 8, 9]
      for (i in interval) {
        // if C_t+1 > C_t+i-x , then ss++
        // Closing price of day tomorrow
        if (close[c + i] > close[c + i - X]) {
          ss += 1  // Increment setup number
        } else {
          // break // This is not needed. If break, then the loop stop afte first setup found.
        }
      }
      if (ss == Y) { // Sell setup is complete
        // lookback and fetch data to plot
        for (i in interval) {
            // Adding to Setup container
            sellSetup["S"+interval[i].toString()] = {"date": t[c+i-1], "close": close[c+i-1].toString(), "type": "sell"}
        }

        // Find whether it is qualified (after sell setup is completed)

        // QS qualified sell setup
        // min (high Y, high Y -1) >= max (high Y - 3, high Y -2)

        // minimum of Y, Y-1  greater than  maximum of   Y-3, Y-2
        var grp1 = Math.min(sellSetup["S"+Y.toString()].close, sellSetup["S"+(Y-1).toString()].close)
        var grp2 = Math.max(sellSetup["S"+(Y-3).toString()].close, sellSetup["S"+(Y-2).toString()].close)
        if (grp1 >= grp2) {
            sellSetup["S"+Y.toString()]["qualified"] = 1  // Add qualified tag
        } else {
            sellSetup["S"+Y.toString()]["qualified"] = 0  // Add qualified tag
        }

        setups.push(sellSetup) // Setup complete

        // Reset ss
        ss = 0
        sellSetup = {} // Container for sell setup
      }
      // max (low, low) >= min (high, high)
    }
    c += 1
  })

  return setups
}

function matchBuySetup(X, Y, rows) {
  var setups = []
  /* Example for debug:
  var buySetup = { "B1": {"date":"04/20/2017", "close": 1600},
                    "B2": {"date":"04/21/2017", "close": 1600},
                    "B3": {"date":"04/22/2017", "close": 1600},
                  }
  setups.push(buySetup)
  */
  var t = rows.x            // Date (GMT)
  var open = rows.open      //Open
  var high = rows.high      // High
  var low = rows.log        // Low
  var close = rows.close    // Last

  var c = 0   // Counter for indexing in to the time series
  var bs = 0  // Counter for each setup interval to determine the size/length
  t.forEach((day)=> {
    // console.log(day + " " + c.toString() + " Close:" + close[c].toString()) // Runing from left to right

    // Check Buy Setup
    if (((c - X) >= 0) && (close[c] < close[c - X])) { // Count possible after X days
      // if C_t < C_t-x then buy setup (BS) = 1
      bs = 1
      buySetup = {} // Container for sell setup

      // For i = 1:Y   // Array of inteters between two values
      var interval = _.range([1], Y + 1, [1]) // use lodash => [1, 2, 3, 4, 5, 6, 7, 8, 9]
      for (i in interval) {
        // if C_t+1 > C_t+i-x , then ss++
        // Closing price of day tomorrow
        if (close[c + i] < close[c + i - X]) {
          bs += 1  // Increment setup number
        } else {
          // break // This is not needed. If break, then the loop stop after first setup found.
        }
      }
      if (bs == Y) { // Sell setup is complete
        // lookback and fetch data to plot
        for (i in interval) {
            // Adding to Setup container
            buySetup["B"+interval[i].toString()] = {"date": t[c+i-1], "close": close[c+i-1].toString(), "type": "buy"}
        }

        // Find whether it is qualified (after sell setup is completed)

        // QS qualified buy setup
        // min (high Y, high Y -1) >= max (high Y - 3, high Y -2)

        // maximum of Y, Y-1  greater than  minimum of   Y-3, Y-2
        var grp1 = Math.max(buySetup["B"+Y.toString()].close, buySetup["B"+(Y-1).toString()].close)
        var grp2 = Math.min(buySetup["B"+(Y-3).toString()].close, buySetup["B"+(Y-2).toString()].close)
        if (grp1 >= grp2) {
            buySetup["B"+Y.toString()]["qualified"] = 1  // Add qualified tag
        } else {
            buySetup["B"+Y.toString()]["qualified"] = 0  // Add qualified tag
        }

        setups.push(buySetup) // Setup complete

        // Reset ss
        bs = 0
        buySetup = {} // Container for sell setup
      }
    }
    c += 1
  })

  return setups
}

function plotSetupTrace(foundSetups, plotDiv) {
  console.log("Ploting setup...")
  // lookup symbol here: https://plotly.com/javascript/reference/#scatter-marker-symbol
  // hoverinfo and hovertemplate
  // https://plotly.com/javascript/reference/#scatter-hoverinfo
  var sellSetupTrace = {
    mode: 'markers+text', name: 'Sell Setup',
    x: [], y: [], text: [], textposition: 'top',
    marker: {
      ypad: 20, size: 0, opacity: 0, symbol: '0',   // make invisible, no marker. Only show text.
      text: {
        size: 14, color: "red"
      }
    },
    hoverinfo: "all", type: 'scatter'
  }
  var qualifiedSellSetupTrace = { // Red arrow above
    mode: 'markers', name: 'QS', x: [], y: [],
    marker: {
      size: 20, symbol: ['triangle-down']  // make invisible, no marker. Only show text.
    },
    hoverinfo: "none", type: 'scatter'
  }

  var buySetupTrace = {
    mode: 'markers+text', name: 'Buy Setup',
    x: [], y: [], text: [], textposition: 'bottom',
    marker: {
      ypad: -20, size: 0, opacity: 0, symbol: '0',   // make invisible, no marker. Only show text.
      text: {
        size: 14, color: "red"
      }
    },
    hoverinfo: "all", type: 'scatter'
  }
  var qualifiedBuySetupTrace = { // Red arrow above
    mode: 'markers', name: 'BS', x: [], y: [],
    marker: {
      size: 20, symbol: 'triangle-up', color: "green",  // make invisible, no marker. Only show text.
    },
    hoverinfo: "none", type: 'scatter'
  }

  // Extract each setup from setups argument.
  foundSetups.forEach((setup) => {
      // console.log(setup)
      Object.keys(setup).forEach((k, i) => {
        var thisSetup = setup[k]
        if (thisSetup.type == "buy") {
          buySetupTrace.x.push(thisSetup.date)
          buySetupTrace.y.push(thisSetup.close)
          buySetupTrace.text.push(k.toString()) // label in key

          if (thisSetup.qualified == 1) {
            qualifiedBuySetupTrace.x.push(thisSetup.date)
            qualifiedBuySetupTrace.y.push((parseFloat(thisSetup.close) - 100).toString())
          }
        }
        if (thisSetup.type == "sell") {
          sellSetupTrace.x.push(thisSetup.date)
          sellSetupTrace.y.push(thisSetup.close)
          sellSetupTrace.text.push(k.toString()) // label in key

          if (thisSetup.qualified == 1) {
            qualifiedSellSetupTrace.x.push(thisSetup.date)
            qualifiedSellSetupTrace.y.push((parseFloat(thisSetup.close) + 100).toString())
          }
        }
        // console.log(k)
      });
  });

  // console.log(qualifiedSellSetupTrace)
  // console.log(qualifiedBuySetupTrace)

  var sellSetupLayout = { annotations: [] }
  Plotly.addTraces(plotDiv, sellSetupTrace)
  Plotly.addTraces(plotDiv, buySetupTrace)
  Plotly.addTraces(plotDiv, qualifiedSellSetupTrace)
  Plotly.addTraces(plotDiv, qualifiedBuySetupTrace)
}
