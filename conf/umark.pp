user { 'umark':
  ensure           => 'present',
  comment          => 'Custom Stock Chart Website',
  groups           => ['docker'],
  home             => '/home/umark',
  managehome	   => true,
  shell            => '/bin/bash'
}

