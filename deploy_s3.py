import logging
import boto3
from botocore.exceptions import ClientError

BUCKET_NAME = "custom-stock-chart"
ACCESS_KEY = ""
SECRET_KEY = ""
SESSION_TOKEN = ""

s3_client = boto3.client(
    's3',
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY)



# s3 = client('s3')

file2send = "public_html/index.html"
file_name = "index.html"

with open(file2send, "rb") as f:
    s3_client.upload_fileobj(f, BUCKET_NAME, file_name,
			    ExtraArgs={'ACL': 'public-read',
					'Metadata': {'mykey': 'myvalue'}})
