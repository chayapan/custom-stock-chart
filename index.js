const figlet = require('figlet');
const fs = require('fs');
const csv = require('neat-csv');
let dataFile = './public_html/data/HSI.csv'

function unpack(rows, key) {
  return rows.map(function(row) {
    return row[key];
  });
}

const raw = fs.readFileSync(dataFile, 'utf8');
const readCSV = async () => {
	const rows = await csv(raw, { columns: true });
	// console.log(rows); // or headers: true

  var t = unpack(rows, 'Date (GMT)')
  var close = unpack(rows, 'Last')
  var high = unpack(rows, 'High')
  var low = unpack(rows, 'Low')
  var open = unpack(rows, 'Open')
  console.log("=== Dates ===")
  console.log(t)

  var setupFound = findSetup()
  console.log(setupFound) // Print JSON
}

readCSV();

function findSetup() {
  console.log("findSetup() ...")
}
